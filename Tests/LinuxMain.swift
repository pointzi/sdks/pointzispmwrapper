import XCTest

import PointziSPMWrapperTests

var tests = [XCTestCaseEntry]()
tests += PointziSPMWrapperTests.allTests()
XCTMain(tests)
